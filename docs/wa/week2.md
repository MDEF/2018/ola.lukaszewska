##**WEEK 02. BIOLOGY ZERO.**
*by Dr. Nuria Conde Pueyo and Jonathan Minchin*


Biohacking. That’s the main topic of class that has taken place during WEEK02. But what actually is biohacking? Does it mean there is a way to outsmart nature? And above all, could we as designers be in fact biohackers as well?

During the Biology Zero class we were introduced to main issues or matters of biology such as what is a cell, metabolisms or DNA. As overwhelming as it was I have the feeling it gave me sort of basic ground for future exploration, some kind of dictionary of definitions. Evolutionary history of life shows how the life was formed - starting from the Big Bang, through formations of molecules to finally conscious creature which brings us, humans, to the point where instead of being just passive elements of evergoing process, we can (finally?) become active actors and participants taking responsibility of changes we have done but also be part of those changes in a way that until now hasn’t been possible.

That brings me to the topic of genetic engineering. Weather or not and to which level it is moral and who is then entitled to decide what is right is one side of debate that will somehow always occur while speaking about modifications of genes. But another side is the range of possibilities itself that humans can face - what to modify? the size or maybe the life span? to make a replacement or to create new skills? In the example, Dr. Nuria showed a following scenario: nowadays scientists are able to modify a genome. Our planet is getting overpopulated and that means the amount of human beings per square meter is simply too high. Why won’t the scientists design smaller humans? Another hypothetical example - it would be possible to make humans gather energy through photosynthesis. The problem is, it won’t give us enough energy because of how big we are. But what if we would be pre-designed to shrink and therefore have smaller mass to produce energy for?

Pre-designed human species. Re-designed person. How does it sounds? At first like a diabolic movie but also like a field of new design. By creating an environment where different fields are cross-referenced and introduced to each other it is possible to create really innovative solutions. Some of them will help medicine, some will solve environmental problems. Biohacking and synthetic biology, if crossed with the mind of designer, may in the future try to actually design completely new animal not by gathering existing pieces but by creating fresh, not yet seen, parts, come up with new kind of sense or additional organ with extra function.

![](assets/BIOLOGY01.jpg)

![](assets/BIOLOGY02.jpg)

During the Lab Workshops few different experiments were performed, including cultivating bioluminoscence vibrio fischeri or lactobacillus, testing the amount of calcium in eggshell and setting up a “mini lab” for growing spirulina. One of the experiments results was exceptionally interesting for me. A classmate made a sample from a root picked up on the street. The spreading-like structure could be spotted. It’s a mesmerizing. I see a mathematical beauty in it. Particular construction with iterative, repetitive geometry - and all that in a way the microbes growth. Seeing this, I started wondering what could be the algorithm behind the process of spreading and if it can me described with parametrical arguments, so the geometry could be then translated into perhaps different scale - or not even - and the strength or other features of this could be applied into design scenarios and projects.

![](assets/HDDM8729.jpg)

Besides asking myself the general “how does it growth”, I note some more specific questions like: is it a context sensitive system and if, how would it react for obstacles? is it the most optimal way of connection or spreading? besides spreading, does the structure have other qualities - constructional ones? is it a space filling system? does it use only the energy that is required for that particular shape’s growth?   if in nature form always fits to function, what is actually a function of such a geometry?

Following the recipe for designing and conducting an experiment, I came up with such a question, hypothesis and research methodology. It’s worth noting that I put my set in a specific framework - biomimetic; by that I mean that the general assumption lays on the claim that the world can be saved or repaired by simply mimicking the nature by treating it “as mentor, not as a warehouse of goods” (Biomimicry Institute).

**QUESTION**: How to create the most optimal connection between given points in the context of transportation?

**RESEARCH**: The right method of research would be applied. In the spirit of previously mentioned biomimetic its methodology could be used:

- defying the challenge - stated as a question above
- biologizing the function or context - how does nature creates connection? how does nature optimize route? how does nature handle traffic? etc.
- discovering nature’s solutions - exploring organisms and systems from biology living matter
- abstracting design strategies - removing “bio” factor, formalizing the solutions
- emulating - creating design strategies
- evaluating - checking the created concepts for how they meet previously stated criteria

**HYPOTHESIS**: In the future autonomous transportation systems (in therms of connections) will be based on dendritic growth structures.

![](assets/BIOLOGY03.jpg)

At the end, should there be a new definition of “nature” when nowadays all you need to redefine it is a hacked lab from open source, basic knowledge and imagination, as the tools are already so available…?
