#Molding and casting


_______________
##Process

To understand the steps I need to take, I had to reverse the whole process:

![](assets/molding1.jpg)

I'm planning to make a cake mold, so the final cast will be the cake (first from the left). First, I have to model the 3D cake. I helped myself with Grasshopper, making a Voronoi pattern on a prism (not a cuboid, but a block with draft angle).

![](assets/cakemold.jpg)

I printed the model (perhaps the quality is too draft), added walls to be able to cast the form (pour the silicon).


At the end few printing tries ended up in a failure and I had to change the model.

![](assets/printingfail1.jpeg)

![](assets/printingfail2.jpeg)

____________
##Model

![](assets/printer.jpeg)

I changed the model to way simpler, build it up in Grasshopper + Rhino and prepared it to print. After printed I decided to leave the rough surface to see how detailed the mold will be.

![](assets/cakemodel.PNG)

![](assets/cakemodel2.PNG)

_____
##Casting and molding

I used  SORTA Clear™ 18 silicone rubber - it's food safe and can be used for culinary applications including casting chocolate and other confections. It cures at room temperature with negligible shrinkage. To start the whole process, I created the walls and base for my model (to be able to pour the liquid inside). Then, I sprayed both walls and model with Release Agent so at the end I will be able to take the 3D object out of the form and remove the walls. To prepare the silicon mix, I had to find information on the [data sheet]((https://www.smooth-on.com/tb/files/SORTA_CLEAR_SERIES_TB.pdf), because the silicon contains two ingredients and they have to be mixed in particular proportions.

- **ratio**: 100A:10B by weight

I weight the A component first and then adjusted amount of component B. It was kind of tricky, because the batch available at FabLab wasn't enough to cover the model, but the workshop run out of the material, so I improvised with stacking some plexi inside in order to raise the level of poured silicon and as a temporary solution, it worked.

![](assets/molding1.jpeg)

![](assets/molding2.jpeg)

Next time, I need to make sure to check FIRST if I have enough of the silicon or other material to pour and check the volume of my box minus volume of my mold. That should give me the amount of necessary mix and even if it's not so precise, I can already judge if I will be able to cover the object completely.


I pour the popsicles orange mix into the form and put it into the freezer for 24 hours.
![](assets/freezing.jpeg)

As you can see on the photo, the texture of 3D print model was embodied really precisely. That is a important thing to know for future experiments with baking and caking.

![](assets/texture.jpeg)


![](assets/Ola_Kuchen.jpg)

____________
##Beautiful example


CATUTS LOLIPOPS
<iframe src="https://player.vimeo.com/video/109478612" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/109478612">Dangerous Popsicles</a> from <a href="https://vimeo.com/user33229803">Bold or Italic</a> on <a href="https://vimeo.com">Vimeo</a>.</p>



____________

![](assets/voronoioverdose.jpg)


____________
#Downloads

DOWNLOAD **CAKE MOLDS** FOR PRINTING [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week09/MOLDS)

Filed called "cake_mold" is the shape I used in here.
