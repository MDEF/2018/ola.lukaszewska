#**WEEK04. Exploring Hybrid Profiles in Design**

##Hybrid profiles
Week 04. was all about meeting designers that works on the edges of different disciplines or at some point of their professional path merged their branches and created somehow fresh or new work environment.
___
##Ariel Guersenzvaig
**PhD in Design Theory in the University of Southampton (UK). Diploma in Advanced Studies in Design Research (Universitat de Barcelona). B.ASc. in Information Management (Book publishing) in HvA of Amsterdam (The Netherlands). His main academic research fields are design methodology, the decision-making in design and design services. As an interaction designer, since 1996 he has defined products and services for big businesses, SMEs, NGOs, and state and government agencies. Apart from his academic work, he also makes projects as an independent consultant in fields related to innovation focused on design and service design.** *(www.elisava.net)*

Ariel, leading us through his career history and the intersections of elements during his job course (teaching, design, research, managing ), was also stating numerous questions and thesis, giving a lot of remarks and hints about design, humanity sciences and critical design. Below are collected notes in generalized form.


**FLOATING THOUGHTS:**

- While speaking about human-computer interaction we do not speak about 0-1 kind of relation. It's a multidisciplinary, hybrid field of design, covering also the topics of "usability" and "ergonomic". "Usability" speaks about subjective experience, soft matter that cannot be shown just by number because of their complexity, to say at least. "Ergonomic" is a term of human factor and that means all that can be covered by numbers, like body measurements.

- "Label changes". With the expansion of first internet systems the development of new careers has started. They were "straight-forward" in a way - like web developer or programmers. But at some point people who wrote codes for websites needed to consider additional factor - their user. Including a human factor in the design of particular site meant focusing on the user and the way he or she experiences and explore given site with its all parts. Such a situation, besides creating new job apart from programmers or web developers, constructed a labels transformation, as there was a need to distinguish what web developer did from a person whose job was to focus on the user's path. And so the names like "user experience design" or "information architecture" started to appear, creating a bridge between many disciplines and/or new factors.

- UX PERSONA + CUSTOMER JOURNEY are two tools used in design. Persona helps creating hypothetical user and target point with description of one's metrics, daily routine, interests and psychological features. Customer journey is a graphical description of experience through time and space.

![](assets/persona.png "exemplary persona profile | source: www.theblog.adobe.com")

![](assets/journey.png "exemplary customer journey mapping | source: www.boagworld.com")

- Decision making in design. "Design rationality revisited" (PhD thesis by Ariel Guersenzvaig)

- There are few existing design methodologies who allows to follow a set of certain rules in order to achieve particular result. Those are step-by-step approaches to the solution. Normally the beginner ought to choose one of the methodologies and, in order to get the best results, obey the rules. But with time, and most certainly with more experience, one can get such a knowledge and sense of different design processes that it's more effective to mix those, use hybrid techniques or even skip the steps and therefore become more elastic in designing. I didn't realize it back then, but now having had the whole week's experience behind I cannot help but notice the similarity between that statement and a part of next meeting when the lecturer was speaking about craftsmanship, on which I will elaborate in the next chapter.

- The tool of understanding the problem by understanding its design. In this approach one need the design idea out of the head so it can be relyed and reflected on. It's a "hands on" way of working - touching and directly working with an "physicallisation" of the idea, seeing it materialized gives the opportunity to work on a concept using different tools than the one that can be used to work with just a thought. Sometimes materialization of the idea allows to solve the problem without understanding the problem itself, but with understanding the design matter.

  Reference: "Sketches of Frank Ghery".

<iframe width="956" height="538" src="https://www.youtube.com/embed/vYt2SQPqTh0?start=230" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**DISSCUSSION**

Part of philosophy is ethic and per se it's already an existing field of research, work and general exploration. Even ethic in design started to appear as the way of defining the role of designer in changes and the way of stating responsibility of designs and designers (my favourite examples in that matter are those two: the way we design materials together with today's buzzword "disposable single-use plastic" - how does it affect the Earth in a long term? - and the way magazines and commercials cover the topics of women beauty and body - what kind of influence have those on how young girls and boys percept it, what kind of a message can they read in those commercials?). But the ethic in design of augmented reality, artificial intelligence and "smart" robotics is relatively new and needs to answer the yet not existing matters - and actually ethic of a given engineering solution is sometimes what can hold a project back as a moral decision is based on so many complicated factors that are hard to translate to the binary language.
During the discussion we covered (well, tried to cover and find an answers) the topics of autonomous driving and killer robots, including the decision making process, responsibility and rightfulness as well as possible system mistakes.


**BOOK REFERENCES**

![](assets/book05.jpg)


___
##Sara De Ubieta
**Completed studies in architecture at ETSAB (UPC) school and got into crafting applied footwear that lead to study the possibilities of non conventional materials through different research projects. She worked as a fashion and product designer beside local factories paying attention to the supplies of materials from different industries, creating different collections. Her projects base the technics and the creation on the agency of the materials as living subject and on the relation between object and crafting.** *(www.deubieta.com)*

Sara welcomed us by telling her story on how the crisis in Spain changed her architecture career and how she decided to keep working, shifted her interest to shoemaking as craft and then to shoemaking with rather experimental way of understanding. She introduced us to the process of creating a shoe, importance of craftsmanship and explain how to read material in order to use its best features.

![](assets/hybrid-2.jpg)

![](assets/hybrid-5.jpg)


**FLOATING THOUGHTS:**

- The magic of craftsmanship, apart from working directly with material, lays in the effect of instant visibility of your work and the ease to translate the idea into object. But here is the similarity to Ariel's methodology topic - at first it's highly valued to understand the core processes in Sara's case the craft of creating the shoe by traditional makers and follow the processes as much as materials. Understanding that path and learning the rules is crucial for breaking them, for future experimentations and break-throughs. With time and experiences comes the ability to mix ways of doing something. But craftsmanship itself is, according to Sara, rather rigid and adamant - it follows certain code of material, it uses certain materials and respects the long tradition, not leaving place for experimental creation. Is this where the line between craft and design could be drawn? Is the "experiment" what determines that line?

![](assets/hybrid-10.jpg)

- Leather, the liveable material. The most interesting pieces among Sara's works (and sure, that is subjective) comes from her trying to understand given material. It's part of doing the art of craft to learn how does the leather work - in what way it stretches, how does it behave in particular movement of feet, which parts have good performance and which one can be used where. She applies those rules - discovering the structure and behaviour of material - to every substance she works with and this points an interesting "problem" that can be seen nowadays - there is a tendency, when a new material is introduced, to try to replace the old one with the alternative and that is simply the mistake, as the replacements are just similar to some particular point, like in the example of leather and it's "vegan alternative", mycelium fabric. Of course those two have same features, like being water resistance, but their structures differ so why the way of using it and working with it should be the same?

![](assets/hybrid-11.jpg)

![](assets/hybrid-12.jpg)

- Material ethic and the choices.


**WORKSHOP**
*Team: Adriana Tamargo Iturri, Julia Danae Bertolaso, Julia Quiroga, Ryota Kamio & myself*

The bottom up design process - from material to the object - is a method that goes well with such a hands-on  approach and such was the tasks. The team was given a material, a piece of compressed textiles rests, that is used for insolation in buildings and a "simple" tasks to create the shoe with a material that is not woven. We decided to take an advantage to understand the directionality of the matter, because even tho it's not woven and not even layered, it has it's own unique structure that can be followed. We used the way in which material is built to either increase its structurality or to create mounting systems for attachment parts. At the end we slipped a part of flat textile to make a socket for feat, leaving an open place for toes to let them breath. As extremely enthusiastic as the effect looks, the processes showed us exactly what Sara meant by understanding the material.

![](assets/hybrid-7.jpg)

![](assets/hybrid-8.jpg)

![](assets/hybrid-6.jpg)

![](assets/hybrid-9.jpg)


___
##Domestic Data Streamers
**We are a strategic communication consultancy firm with a mission to tackle the challenges society faces with storytelling through data.** *(www.domesticstreamers.com)*

![](assets/datas-5.jpg)

Domestic Data Streamers is a studio that lives in the world of data. They described themselves as "designers who try to make art". And it's simply amazing what kind of art they create - DDS found a playful, often engaging way to not only show information, but also to actually gather them. They show how many information are around us and that there are multiple way to translate and interpret them.
DDS team is a mixture of people coming from different backgrounds - obviously not only from different design branches, but also from completly different fields - engineering or art.

<iframe src="https://player.vimeo.com/video/75655845" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


**FLOATING THOUGHTS:**

- By creating playful, engaging way of gathering data it's possible to change the approach of understanding information.

- Every single human being is already a huge carrier of information and data itself - from simple things like gender, name, age, race, through more cognitive, like spoken languages to completely complex like emotions or certain connections. When a human interacts (with someone else, with environment, with an object, with an experience) such an act is another source of new set of inputs. If this behaviour happens within small community, like team, the whole net of information stream creates itself. The tricky part is to extract information out of this but once it's done it shows a unnoticed sculpture of world, patterns of connections. It amazes me how easily it is to change the way the world is seen by showing links between actors that haven't been noticed before.

- The way of communicating and visualisation of data can be art (or design; also - what makes one artist or designer?). Apart from what Domestic Data Streamers do and how they show it, there is this beautiful project of postcard crossing: each postcard is in fact a data diagram - the undertaking is called "Dear Data" (*www.dear-data.com*).

![](assets/deardata01.jpg)


<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/david_mccandless_the_beauty_of_data_visualization" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>


**WORKSHOP**

![](assets/datas-3.jpg)

Task: empty your bag and show what you have there! Use the items to tell a story, show information and translate them into data stream.

My things were: wallet, notebook, pencil case, water bottle, magazine. I've realized all of them were in my bag because at some point I needed to buy a "newer" version of particular object. And so I am a telling a story of replacements and emotional attachment, if the item was bought out of need, in a way that a person requires time to - if at all - create an emotional bond with given object. It's also interesting perspectiv to look at possesions - as the things that at some point will be worn out or destroyed and will need to be changed.


![](assets/datas.jpg)


![](assets/datafrombag01.jpg)

___
- *Notebook by Iconic from the Noun Project*
- *magazines by Made from the Noun Project*
- *Water Bottle by Rudy Jaspers from the Noun Project*
- *Wallet by Rockicon from the Noun Project*
- *iPhone by Mundo from the Noun Project*
- *pencil case by blemmie from the Noun Project*


___
##Reflection. Vision. Future me.

What is a vision? We defined "vision" as a cluster of following keywords:

* prediction
* goal
* path to follows
* driving force
* scenario
* authenticity
* framework
* coerce
* inspiration
* commitment
* perspective

The work on self development and reflection on past three meetings with hybrid designers got a structure as such:

1. Try to compare yourself and use it as a tool of understanding your abilities.
2. Try to find points of attraction and repulsion in the knowledge, skills and attitude.
3. Try to make a list of strong features and needed improvements.


I've created the mind map of actors writing down all the points I've remembered, felt, or experienced during studio meetings. Full graph may be seen [here](http://arborjs.org/halfviz/#/MTIyNzI).

As for now, I see all those points as anchors, points of attraction. I feel that the bridge can be drawn between two random nodes and it will create an interesting hybrid worth exploring. I'm trying to narrow down my focus points, but I am not yet at the point of choosing only few just from this particular map.
![](assets/graphscreen-01.jpg)
Keeping the attractors in mind, I went back to trying to understand "who am I" in terms of my abilities, skills or strengths and noticing fields that needs improvements. Surprisingly or not, it was way harder to list strong points rather than what I know requires intensive development and further hard work. It's obvious for me that I need to keep learning and "good at" doesn't mean being expert or being extremely confident about particular ability. It means those fields right know require less intensive effort than the one named as "need to learn that".

![](assets/skills.jpg)

A big part of identity and future vision should be as well fields of interest, things I believe in, things I simply enjoy making in the context of work (but as well slightly around). And so I've tried to write down main topics / areas / fields for which I have a feeling of wanting to give my attention to, being involved with and discovering more about. Then I tried to sort them by what I want to achieve or in which part I want to put my focus on.

Preview of the graph after clicking on the image below.

<a href="https://ibb.co/hTSctV"><img src="https://thumb.ibb.co/hTSctV/INTEREST-GRAPH-02.jpg" alt="INTEREST-GRAPH-02" border="0"></a>



Cambridge Dictionary gives a definition of a word vision as "an experience in which you see things that do not exist physically, when your mind is affected powerfully by something". My interests are my driving forces, inspirations or just appeared there during my education process and I hope they will keep affecting me powerfully and push me into "right" direction, which in this case would be that kind of work that is not only fulfilling and gives joy, but can also contribute to something (weather it's social, environmental or cognitive impact).

There is obviously a lot of generalized terms and all of them could be both narrowed down or deconstructed into narrower topics. I believe I can work in the intersection of some of them, using others as tools or methodologies, getting inspired by others or translating process (for instance) from one area into another.


##Me in 5 years

During the class assigtment, writting down our "future vision" in a simple, understandable manner, I noted down those words:

*Distributing design knowledge and skills through small scale workshops (both as space and experiences) to give children new tools for/of perceptioning and impacting the world - even if it's their own world around - new kind of "hands on", material embodied knowledge, learning by doing, because of lack of changes in education systems.There are two paths - on one side - being a part of a bigger network and system, on the other - creating tools, spaces, machines itselfs. Important part of such an undertaking would be good team*

I can picture myself creating such a space, a physical platform - for educating by doing and learning by really understanding - in the next 5 years. I wasn't convinced to this "vision" during class completly and I am even less convinced to it now. Not because it doesn't match my interests or I don't believe in the rightfulness of it, but rather because I see it as a single idea, one point in the whole cloud called "vision".


##Development plan

My development plan for next weeks is to either create more generalised vision of myself that includes more of my interests and skills, map the changes after new weekly seminars and update the description below this post or create more narrowed ideas like the one from class tasks. Or both. Constant inputs, new experiences and pure knowledge that we are subjected to here at IAAC creates an enormous amount of outputting thoughts and a wide range of emotions. I went from a really lost state of mind to feeling calm because I finally got to narrowing down and creating my future self in really specified words to end up by feeling even more lost than at the beginning but also calm and curious about future tasks at the same time.

I believe that knowing what keeps me going, what topics allow me to just dive in deeply in the project and get involved and what fields I may merge can help me create a really well described vision, but also define my identity as a designer.
___

*For future notes:*
Weekly reflections.
Understanding what the classes can bring - what are they comparing to what I need to improve?
Start deconstructing the plan in order to know when I need to push stronger and be more focused, put extra effort.
For example:
"engaging with narratives" would be an important class for me, because I want to explore ways of communicating and learn methods to be able to improve my communication skills, visually but also in writing and probably most importanty in speach.


Goals for next seminars:

* reflect weekly on how given topic, meeting, lecture resonates with me, my interests, my vision.
* write it down, as the written word has more "power"
* deconstruct the plan and make a specified list of "I want to.. because.."
* work on my vision and identity

____
