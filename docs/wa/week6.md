#**WEEK 06. Designing with extended intelligence**
*by Ramon Sangüesa and Lucas Peña*

___
##The Cyberiad stories

*One day Trurl the constructor put together a machine that could create anything starting with n. When it was ready, he tried it out, ordering it to make needles, then nankeens and negligees, which it did, then nail the lot to narghiles filled with nepenthe and numerous other narcotics. The machine carried out his instructions to the letter. Still not completely sure of its ability, he had it produce, one after the other, nimbuses, noodles, nuclei, neutrons, naphtha, noses, nymphs, naiads, and natrium. 'This last it could not do, and Trurl, considerably irritated, demanded an explanation.*

*"Never heard of it," said the machine.  
"What? But it's only sodium. You know, the metal, the element..."
"Sodium starts with an s, and I work only in n."
"But in Latin it's natrium."
"Look, old boy," said the machine, "if I could do everything starting with n in every possible language, I'd be a Machine That Could Do Everything in the Whole Alphabet, since any item you care to mention undoubtedly starts with n in one foreign language or another. It's not that easy. I can't go beyond what you programmed. So no sodium."
"Very well," said Trurl and ordered it to make Night, which it made at once - small perhaps, but perfectly nocturnal.*


The fragment quoted above comes from the collection of "The Cyberiad" stories, written by the Polish writer Stanisław Lem. Lem is my first contact with cybernetics, robotics, futurology and "phantomics" (today known as virtual reality). As a child, I considered these stories to be fairy tales about robots, beautiful journals from interstellar travels, to be the world of sci-fi, where machines can create alternative realities, dust can be smart, and the memory bank is a computerized network.

Little did I know that 10 years later at a lecture in Barcelona I will listen to nothing but intelligent machines, robots that can learn and a huge virtual database.


___
##AI - a data driven world

Information, understood as data, is extremely relevant - apart from strictly cognitive properties, it allows us to create patterns, connections and dependencies. Data help in understanding all repeated behaviors. It's information that is omnipresent (and can be shaped differently, but that is another part of story).

Programming means coding command lines, an algorithm on which a given machine or program will depend and could follow. In a way, Learning Machine bypasses that part of the coding that relies on commands and observance of the rules entered. In opposition, it collects the data and provides a prediction by analysing it. And that's precisely where the validity of the data themselves is visible - depending on what data will be delivered to the machine, this machine will learn. The more data, the more accurate and precise the prediction will be.

A date is something known or believed to be known as facts that serve as a basis for creating reasons or calculating. It is a philosophical definition in which the phrase "considered as known as facts" is an important part. Intelligence, which is fed with information, can not (yet?) Value them in the sense of ethics or morality - it only knows certain patterns and relies on them, and also knows the data it has analysed before. This situation is created not only by the question about the morality of artificial intelligence and decision-making behind the process, but also about the ethics of "data feeding".

A good example here is the history of false fingerprints. Research scientists from NYU based on the study of the so-called "master print", i.e. a print that contains features of various common fingerprints, and creates a generic formula c for unlocking biomimetic castles. More about the project - [here](https://www.wired.com/story/deepmasterprints-fake-fingerprints-machine-learning/?fbclid=IwAR3Fa2tEu2LIv2UvBj89fMUlJb486xqm0r-mSZNXALsBLFJrk7FqO7RCgn4&mbid=social_fb&utm_brand=wired&utm_campaign=wired&utm_medium=social&utm_social-type=owned&utm_source=facebook).

Who is responsible for providing and hierarchization data? Does the machine know that the data it has received is not reliable or incorrect? Can the unreliability and impropriety of data affect people and can be used as a tool for manipulation, is this topic that we should be afraid of?

On the other hand, even today, without the ubiquitous AI, we are constantly manipulated by information that reaches us - just look at the media - the way the data are given and their selection cause that we do not receive pure information, we read it as we are wanted to read them. But we are already aware of that. In the case of artificial intelligence whose source data may be manipulated, is the awareness of such a causal enough to defend against it? Is the very fact that the machine needs to get data but can not choose them selectively (in the sense of awareness of choice and decision making) limits its possibilities? As in the introduction - Trurl's machine could do all of whatever was wished for as long as it started with the letter "n".


___
##AI as a tool

The work of human hands in any reality over time (most often) was associated with the use of a tool. A hewn stone, an ax, a mechanical saw. Ancient abacus, calculator, mobile phone. These objects, along with time, evolved, became more precise, maybe more advanced, more comprehensive. Despite all this, they were passive tools throughout - the hand of a man who was doing the act was needed. The tool worked only when and as it was directed.

Artificial intelligence is a contrast to this. It's a new tool for shaping reality. Additionally, it doesn't have to have a physical form to be doing or making. He is an active actor of activities. AI is not yet independent, not self-aware, but the activity does not require - in a generalization - the human hand, and that is the transition from passively used tool into active member of a work.


___
##Floating thought
I think that as usual, especially among people with limited knowledge on a given subject, just like me, there is a tendency to create either beautiful utopias or macabre dystopias - either the world in which we live with robots and intelligence in harmony or the world in which AI has taken control of people and plans to extract them. Reality, as it usually happens, lies in between. It'll be interesting to see in which direction the scarf will be tilted - because it seems to me that hardly anything in our world remains neutral.


___
##Team activity: happy pigs
*team: Nhu Tram Veronica Tran, Fífa Jónsdóttir, Ilja Panić, Vasiliki Simitopoulou & myslef*

![](assets/pigpig.jpg)


**Define your intelligent object**

GOAL:
Measuring happiness of pigs in order to know the quality of proceeded pig (with the assumption that happy pig = better life quality = better farming conditions = better, cleaner meat quality) - to make people aware and let them be informed, make supermarkets more transparent

TASK:
determine happiness and put it through a matrix

INPUT:
sounds, face expressions, micro expressions, heartbeat, chemical compounds of smell, level of hormones like dopamine and cortisol

OUTPUT:
psychological and environmental data, behaviour patterns

CONTEXT OF CREATION:
farms + supermarkets

INTELIGENT OBJECT:
ecosystem and net of measurements, matrixes, platform, algorithm.


**Identify your “intelligent objects”**

METHOD - how we measure the pigs happiness?
+ learn to “read” inputs one pig
+ learn how to recognise and measure happiness (or rather measure it and give an output as a level / numeric value)
+ learn how to read given signals  and pair them (cortisol level vs dopamine level, behavioural pattern + farm free land per pig, etc)
+ learn to pair pigs in order to increase happiness (what is increasing the level of happiness? sad + sad, happy + happy, happy + sad?)


**What learning method would we use to teach our system?**

Every pair or group of input would require different learning method, and they would need to be at some point prioritise and run again as pairs through different levels of learning methods - depending on the type of data that will be collected - numerical, chemical, graphic representation, etc.


**Ethic - what ethical problem may we face?**

- What is happiness?
- When is a pig happy enough to be killed?
- Is it ethically and morally better to kill happy pig, knowing it had good life, or unhappy, knowing it will die anyway?
- Who decides when the pig should be killed and who determines  the level of happiness?
- Is it morally proper to design the system knowingly pushing the responsibility to the farm owner or meat requester (he?) of determining the “that’s it” level and therefore killing an animal?
- Should we design the intelligence to try to improve the life of pig knowing it is done only for a better meat quality after slaughtering?

What effect of the knowledge how happy the pig was would be on the customers? Maybe it would decrease the amount of sold meat and that would be our reason to design it at the first place, what if it goes the other way around? What impact the transparency of the product would have on the animals itself, on the customer’s decisions? Would it be possible for breeders to “hack” the system or feed it false data in order to have a stamp or sticker of “happy meat”?

___
**Wrap-up**

To complement the teachings of the week, we designed an ‘intelligent agent’ for defining the metrics of a pig’s happiness, that was to subsequently be outputted through a ‘happiness rating’ on meat packaging. It helped inform us on how we go through the steps of producing an AI through philosophies and metaphors, that flow through throughout more complex societal ideas, paralleled with use of existing technologies and data.

The goal of our agent was to inform the purchasing decisions of a person by means of adding transparency to the shopping experience. How do we determine, convey, and then show a metric for the happiness of a pig? The complexities of designing such a system were unravelled as we dived into the process of defining inputs/outputs, contexts, methods, and technologies that would combine to make up such an ecosystem.

We realized at one point that we had discussed the happiness of pigs without discussing the slaughtering of the pigs, leading to additional questions; does our system then decide at which stage of unhappiness will it be moral to kill a pig? Is it better to kill a happy or unhappy pig? How do we design with social biases in mind, and account for the qualitative vs quantitative nature of the variables, with the examples being – the chemical reactions i.e measurement of dopamine or cortisol levels in contrast to less measurable data, for instance the life and environmental factors of the pig.

Can we tap into the vision of a pig to measure its excitement when it sees mud?!

How do we turn qualitative knowledge into quantifiable data to automate the process of ‘measuring happiness’?

It was simultaneously enlightening and challenging to convey our thought processes by connecting floaty ideas with existing technologies, and I found it a really fun way to ‘apply’ the knowledge of the class into a design. The Google Clip project was also another project worth noting when discussing a machine’s ability to define happiness.



___
##Generative design

Between the ends of the "active" and "passive" scales there is also "generative".

Generative design does not work independently, some information, constraints or frameworks must be given and entered into an algorithm that is a programmable work path, but discovers a set of possibilities in space, sometimes giving unexpected results. This piece of the road between "generative" and "intelligent" is the phrase: generative design is unable to learn.

One of the tools for code-based generative design is Grasshopper. Proving Ground has launched a plugin that allows you to write scripts having Machine Learning - for example, it allows you to generate solutions, dodge and neural network within computational design. I start the tests and the first scripts and I will update this entry soon.



*An example of generative design in structural elements, optimising material usage while keeping the strength features - click on the image for more information.*

[![](assets/generative.jpeg)](https://www.arup.com/news-and-events/news/3d-makeover-for-hyperefficient-metalwork)


___
##collections

<iframe width="560" height="315" src="https://www.youtube.com/embed/X8v1GWzZYJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

https://www.theverge.com/2018/11/1/18051196/ai-artificial-intelligence-curiosity-openai-montezumas-revenge-noisy-tv-problem

https://www.theverge.com/2018/8/21/17761424/ai-algorithm-art-machine-vision-perception-tom-white-treachery-imagenet
