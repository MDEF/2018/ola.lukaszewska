#Project management


____
##Creating FabAcademy subpages

To enter new cards and subpages, I had to edit the "mkdocs.yml" file, which contains information and addresses of (sub)pages. At the beginning, this operation didn't work for me because I had a duplicate file in my folder. After cleaning the path, I could start adding new tabs.


![](assets/fablab01.JPG)


This is the structure of the "Term 1" page and the construction of this file in general. By copying such a structure, I could have similarly styled new cards for the 2nd trimester and a new page for Fab Academy, with later division into weeks and discussed topics.


![](assets/fablab02.JPG)


The next step was "copying" this structure and rewriting it for the wanted subpages. First enter the name (title) to be visible on the page and then after the colon name of the .md file, i.e. the file that contains the content, gives the page - *Title to be shown on website: wa/filename.md*


![](assets/fablab03.JPG)


Then I had to actually make the files of the pages in which I can later enter the content. To do this, create a new file (Ctrl + N), save it (Ctrl + S) in the appropriate folder (in the path that we declared in the previous step) and give it a name with the extension (again, the name that was declared in the previous step).


![](assets/fablab04.JPG)


Now I only had to save, commit changes and finally click "push" to apply new parts to the website.

Such operations, which I described above, didn't work for me - I couldn't see all of wished tabs on the website, therefore I assumed not all subpages were created properly. I noticed that one of probable mistakes (I think, though I do not know for sure) was to broadcast files for titles that were just a kind of menu to develop, under which the right pages would be hidden, like "Term 1". The changes are visible on the screenshot attached below.


![](assets/fablab05.JPG)


What finally worked was the arrangement of spaces, empty spaces (margins?). I copied them from the first trimester and so the menu was completed.


![](assets/fablab06.JPG)


It was not the only problem. Although the menu looked as if the changes in the file worked, something was still wrong. After clicking on the subpage, such a view was shown.


![](assets/fablab07.JPG)


As I was pointed out by my colleague, the error was in a way really trivial. The file path wasn't correct - I just provided the name of the .md, not the folder in which the file is located.


![](assets/fablab08.JPG)


____________
##Uploading files

One of the points on FabAcademy to-do list is also adding the files for future uploading. In order to learn how to do it, I tried to find an instruction on how to do that explained in a understandable for me way, as I am still not completely comfortable with the "lingo". I followed [this](https://help.github.com/en/articles/adding-a-file-to-a-repository) step-by-step tutorial.

On my main repository page I clicked on the icon marked below to find the "Upload File" command.

![](assets/uploading-01.jpg)

Then, the "Upload File" window appears. Files can be choose by browsing or by drag-and-drop.

![](assets/uploading-02.jpg )

After choosing a file and adding it to the window, the comment for acction can be given. Once this is done, it's all ready to click "Upload"

![](assets/uploading-03.jpg)

At that point, I was a little bit lost, as this window appeared and it wasn't included in the tutorial. I simply accepted all that was given there.

![](assets/uploading2-04.jpg)

I accepted the window. Then, I looked for a command that would help me either embed or link the file to download, but I couldn't find it. For now, I am basically linking the url of given file, although I am not sure if that is a proper way.

**CLICK [HERE](https://gitlab.com/ale_lukaszewska/docs/blob/master/trump_floss.gif) TO DOWNLOAD** and enjoy.

During this task I also learned a little bit about Markdowns ([here](https://gitlab.com/help/user/markdown)). I am still running on the initial website, without customized layout. I find it quite challenging to move through Atom, Git and Co. I am planning to eventually move into better looking layout on my website, but that requires a lot of time, which in the next, upcoming days might be a luxury. Anyhow, at some point, I am going to give this dowloading files process another go (unless that turns out to be a right way).

![](assets/trump-test2.gif)


____________
##Creating new folders in repo

Having learned how to upload the files into my own repository, I wanted keep the files organised for every week and so I have to learn how to make new folders, becuase it cannot be easy-peasy and just creating folders on my own disk. I watch [this](https://www.youtube.com/watch?v=rVNFPj9jtb0) tutorial and also read [this](https://stackoverflow.com/questions/12258399/how-to-create-folder-in-github-repository) thread on forum.

**TO CREATE NEW FOLDER:**

- go to your main REPOSITORY website
- choose the "+" icon and click NEW FILE
- in the file path write the name of wanted folder, eg. docs/FabAcademy/week02/
- add an empty file, eg. /empty
- in the editor field, add some random text, eg. ##empty
- add commit message
- click COMMIT CHANGES

![](assets/newfolder1.JPG)

One of the things I did wrongly on the beginning was to add a wrong-sided slash - just remember the one that is usually used in the adress paths. the **/**.

![](assets/newfolder2.JPG)
