#Electronic design

Assignment: redraw the echo hello world board, add a LED button and test it.

____________
##EAGLE
For this task I followed the step-by-step class that was given us by [Rutvij](http://fab.academany.org/2018/labs/fablabvigyanasharm/students/rutvij-pathak/).
We learned to use EAGLE, software by Autodesk, a scriptable electronic design automation application with schematic capture, printed circuit board layout, auto-router and computer-aided manufacturing features. We followed Rutvij's instructions, starting the whole process from marking what will we need to construct the board. Similarly to soldering, we started from the microcontroller ATtiny 44, connected the GND ground and VCC power supply, then we continued with other necessary parts, like LEDS and compatible resistors, button and jumpers, connectors etc. And with that we came to a whole schematic of the PCB with LEDS and button.

I also later read and helped myself with this [tutorial](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week6_electronic_design/eagle_english.html). It contains all the explanations on elements and components on the board together with their function and meaning. Really usefull to read before designing the board.


**Schematic** in electronics is a drawing representing a circuit. It uses symbols to represent real-world electronic components:

![](assets/eagle1.PNG)


**Board** the actual real-world board that will be milled:

![](assets/eagle2.PNG)

_______________
##Exporting files
To export the files, it's necessary to turn on specific layers for each file.

- **EXPORT FILE:** File > Export > Image
- **LAYERS:** View > Layer setting
- **SETTINGS:** .png, 1000dpi, enable monochrome style
- **TRACES:** turn on the TOP and DIMENSION layers
- **OUTLINES:** turn on the DIMENSION layer

After exporting the files here are the outcomes:

![](assets/outlinecut.png)

![](assets/tracescut.png)

And for soldering and collecting the parts, the schematic:

![](assets/schematic.png)

____________
##Milling

Once the outlines and traces were ready, it's time to moved to milling. Yet again I used settings from week 4:

- Board traces has to be milled first and by using the fab modules and a 1/64" bit.
- Board outlines followed with the fab modules and a 1/32" bit.


Before milling file need to be prepared, where we define input and output data (like machine, speed etc.)

![](assets/Captura.JPG)

![](assets/Captura2.JPG)


Then the file can be open in the Roland mill software:

![](assets/Captura3.JPG)


____________
##Components

***Components list:***

- 1 ATTiny 44 microcontroller - solder the microcontroller second. There is a marked circle on the microcontroller - that's the VCC pin.
- 2 LEDs - diodes have polarity, so orientation does matter; cathode side is marked with a small line, so the side with the line should be placed where the "C" is marked on the diagram (grounded).
- 1 Capacitor 1uF - marked with C, capacitors don't have polarity, orientation doesn't matter, they are not marked on the parts.
- 2 Resistors 499 ohm - marked with R, resistors don't have polarity, orientation doesn't matter.
- 2 Resistors 10K - marked with R, resistors don't have polarity, orientation doesn't matter.
- 1 Jumper: 0 ohm resistors
- 1 6-pin programming header, orientation doesn't matter.
- 1 FTDI 2x3 header
- 1 resonator 20MHz


____________
##Board

![](assets/DSC04215.jpg)


____________
##Files

  **DOWNLOAD FILES [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week06/ELECTRONIC%20DESIGN)**
