#Electronic production

_______________
##PCB

**What's a PCB?**
A printed circuit board (PCB) mechanically supports and electrically connects electronic components or electrical components using conductive tracks, pads and other features etched from one or more sheet layers of copper laminated onto and/or between sheet layers of a non-conductive substrate. Components are generally soldered onto the PCB to both electrically connect and mechanically fasten them to it.

The Electronics Production assignment is to mill the board, stuff it with components and program it.

I followed step by step [tutorial](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week4_electronic_production/fabisp.html)


**Board traces:**
![](assets/traces.png)


**Board outline:**
![](assets/interior.png)

**Board schematic:**
![](assets/componentsschematic.png)

______________
##Milling

- Board traces has to be milled first and by using the fab modules and a 1/64" bit.
- Board outlines followed with the fab modules and a 1/32" bit.

______________
##Components

Then, I followed the FabISP labeled board diagram to know what components are needed and where to place them:


***Components list:***

- 1 USB connector - soldering the miniUSB first, as it seems to be the most tricky part to attached to the board.
- 1 ATTiny 44 microcontroller - solder the microcontroller second. There is a marked circle on the microcontroller - that's the VCC pin.
- 1 Crystal 20MHz - marked as 20 MHz on the diagram, crystals don't have polarity, so the orientation doesn't matter.
- two Zener Diode 3.3 V - diodes have polarity, so orientation does matter; cathode side is marked with a small line, so the side with the line should be placed where the "C" is marked on the diagram.
- 1 Capacitor 1uF - marked with C, capacitors don't have polarity, orientation doesn't matter, they are not marked on the parts.
- 2 Capacitor 10 pF - marked with C, capacitors don't have polarity, orientation doesn't matter, they are not marked on the parts.
- 2 Resistor 100 ohm - marked with R, resistors don't have polarity, orientation doesn't matter.
- 1 Resistor 499 ohm - marked with R, resistors don't have polarity, orientation doesn't matter.
- 1 Resistor 1K ohm - marked with R, resistors don't have polarity, orientation doesn't matter.
- 1 Resistor 10K - marked with R, resistors don't have polarity, orientation doesn't matter.
- one 6 pin header - marked J1 ISP is the 6 pin programming header, orientation doesn't matter.
- 2 jumpers: 0 ohm resistors - components marked SJ1 and SJ2 are solder jumpers.
- one USB mini cable
- one ribbon cable
- two 6 pin connectors


NOTES:

**Resistors:** mark on the diagram - value - mark on the component

- R1 - 1 K ohm - 1001
- R2 - 499 ohm - 4990
- R3 - 100 ohm - 1000
- R4 - 100 ohm - 1000
- R5 - 10k ohm - 1002

**Capacitors:** mark on the diagram - value

- C1 - 1 uF
- C2 - 10 pF
- C3 - 10 pF

![](assets/components.jpg)

_______________
##Soldering

I soldered the components as it was suggested in the tutorial and as I was instructed by [Rutvij](http://fab.academany.org/2018/labs/fablabvigyanasharm/students/rutvij-pathak/). I used the desoldering braid to wick solder blobs away.

![](assets/desoldering.jpg)

![](assets/DSC02675.JPG)

![](assets/DSC02679.JPG)

_______________
##Board

![](assets/DSC02684.JPG)

![](assets/DSC04211.jpg)

_________________
##Programming
