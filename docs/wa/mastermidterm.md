#Master - mid-term presentation

____________________
##Presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSUMPiCGaRq2EK_iYsi4DGALoRmYNs3x4TYlHP-QlQVP6YUpLJqMZu3HTAscExgY9OztzBJ11nTZ0gB/embed?start=false&loop=true&delayms=3000" frameborder="0" width="1440" height="600" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

___________________
##Comments

**PAGE 1**
The structure from the photo is the result of parametric workshops from 2016. At that time, we were designing a pavilion for a theater festival, which was later to be put in a park, but before fabrication started it turned out that the amount of material is smaller than we originally assumed, so to fit in time, we scaled the structure and adjusted it to the available sheets. After the fabrication and assembling, we constructed it in the park and organized the opening - although it may be too much said, several guests, including families with children, showed up. When I remind myself of this situation, I always think that we, makers, "the adults", have seen this building still the same way as we thought up at the beginning - as a pavilion, a place to enter, walk around, hide under in the shadow, so the usable part was what was under the roof and the thing that stand in front of us was rather a form of a prototype made in a smaller scale but not our finished object. However then children entered the situation - they saw the thing for the first time, so they interpreted it in their own way and turned it into a playground - and although one of the boys actually rolled in and hid under the constructions, most of the kids climbed on the pavilion and jumped on or from it.


**PAGE 2**
Since then, with growing curiosity and interest, I have been observing how children interpret the world around them, what situations are challenges for them and how they can give new narratives to objects and situations. In connection with this (and, of course, the outcomes from Design dialogue session and general I term), I began to ask myself how we can celebrate the natural curiosity that children have in them. Children, especially the youngest, have an innate need to learn how the world around them works. It drives them to explore, search and ask. And as it turns out that the strongest tool that can be used in education is their ever-renewed question "why?".

Drawing from my own experience at school, I know that in the educational process the question "why" is changed into "which answer is correct". Schools teach how to prepare for testing, not to satisfy curiosity, to gather knowledge and to put it in use. I do think sometimes that through the educational process the creativity is killed among many children ( and also actually it isn't taught to learn the process - it's rather common to noticed and show that the road isn't important at all and yet only the result is). So I ask myself another question - if yes, how can we expand and improve the creative sensibility in children?


**PAGE 3 - 4**
The key idea at that moment is to design a "bio tool kit" and that is a sort of "do it yourself" synthetic biology experiments resulting in effects from biodesign field. The set should be of course appropriate for children to use, so the hazard or any other health risk cannot be included and will play a huge role in the whole process. The aim in not only or rather not necessarily to teach biology, but to give span tool across science, design and craft in order to embrace multiple development areas and let the kids be the ones who bring material, and for them a "toy", to life, be an important part of the process that they can steer. The general idea, although of course not precisely, is to add the "B" to the STEAM - as funny as it sounds, it's about adding a fresh educational approach to existing, but sadly still emerging ones.


**PAGE 5**
The topic came at first from my own interest and the first term workshops; combining it with the children education was a direct outcome of design dialogues so the decision wasn't driven by one question such as "what is the missing piece". But what is the potential of bio-design? If you look at projects that are created in this field, many of them tell or use such possibilities of bacteria as the ability to grow, and thus create some matter - for example kombucha, as the ability to create colors, and thus coloring, luminescence and other equally attractive features and opportunities that have the potential to stimulate creativity and critical thinking while being the part, steering and observing the process, sometimes even without full and total control.


**PAGE 6**
At this moment that's how my overall work plan is presented. Desk research is the first step that's necessary to start parameterization, that is - to analyze the methodology of similar projects and ventures about children's education but from other fields such as programming or robotics and additionally other educational interventions and undertakings. This, I hope, will help me create a lexicon of parameters or the elements I need. The entire project has no chance of being created if it isn't consulted with experts in fields, especially in the biology area. This is one of those key points / actions that should be done in parallel with other stages, but at some point, I imagine that before planning what exactly this set will contain, during the generation of concepts, will be a key issue. The next steps, that is prototyping, testing - at some point already with the children - and up-grading is a continuous loop, not a linear process and will require constant steps back, feedbacks and applying the changes.


**PAGE 7**
In the current phase of desk research, I look not only at the previously mentioned "similar" projects from other fields, but also gather information about educational approaches, such as STEAM education or Montessori schools, educators and pioneers, such as Michael Resnik or Paulo Blikstein, creative science centres, like the Copernicus Science Center in Warsaw, Poland or Ars Electronica Centre in Linz, Austria. I'm creating a list of references, resources, methodologies and motivations. Probably by the end of the trimester I will publish them in [REFERENCES](https://mdef.gitlab.io/ola.lukaszewska/wa/references/).


________________
##Feedback

**Here is the summed-up list of main feedback thoughts I was given during the session:**

1. Have a strongly defined "why" in the context of biodesign, as that is the core question to be asked and among all the "whys", try to find my own drives among it all.
2. Think about biodesign as about a highly political action (which at first I did not completely understood, but I was later on filled in during the session with Mariana); look into the biohacking history, community, particularly into bio- and hacking ethic.
3. Define and be aware of the weak signals and "what's wrongs" of todays education system and methods and also point it out while speaking about the topic, be critical towards current programs and the delay that is happening.
