# Process

![](assets/1.png)


**TEAM: [| Veronica](https://mdef.gitlab.io/veronica.tran/) [| Jess](https://mdef.gitlab.io/jessica.guy/) [| Emily](https://mdef.gitlab.io/emily.whyman/) [| Gabriela](https://mdef.gitlab.io/gabriela.martinez/) [| Alex](https://mdef.gitlab.io/alexandre.acsensi/)**

____________
##The Make Works

![](assets/makeworksscotland.JPG)

At first we were introduced [The Make Works](https://make.works/scotland/), a project (platform) aiming to make manufacturing accessible. It's a library of open resources made by factory finders where people can discover skilled manufacturers, tools and materials to make work with in given area. The idea was to learn about The Make Works, look into their (quite detailed) methodology and requirements and simplify it in order to fit into a one-week project held in Poblenou, Barcelona.

_______
##works make

![](assets/est030933.jpg)

_______
##questions

![](assets/est031048.jpg) (rowery)

______
##The Makers

![](assets/est030021.jpg) (ludzie)

________
##visits, interviews

![](assets/DSC00363.JPG)

__________
##preparing material


__________
##Poblenou

<iframe src="https://www.google.com/maps/d/embed?mid=1fCCozL_TB9qfe-5chTqKAyr-0xXDkoAc" width="640" height="480"></iframe>
