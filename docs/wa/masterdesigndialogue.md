# Design Dialogues
*presentation and reflections after term review and feedback session*


___
##Term 1. Useful tools

Term 1. was a story about getting loads of inputs. Every week has brought something to reflect about (even if that wasn't happening right away and sometimes I was left with the feeling that I can only receive the input but not really process it right away - the reflection is somehow happening now, at the beginning of second term). However, some of the tools in particular helped me form my first narratives about research topic.

The Biology Zero week caught my attention, I was curious and fascinated how it can be to work with live matter. Also, one of my initial area of interest was biomimicry, bionic and generally getting inspired and seeking in nature, its solutions, tricks, structures and more. So here it was, mine (still quite wide) focus point.

I was aware I am not in the phase of ideation just yet. I needed to explore such a wild field and to do that I repeated the exercise we did during speculative design workshops - I created some artefacts mimicking (just by look and aesthetic) biology processes or bacteria in lab containers.  And so I just allowed the questions to come, starting all of them from "what if". At the end I had a variety of points that could be explored separately and every one of them could become a topic on its own for a project.

As far as the exhibition itself goes, the week with storytelling was really helpful. I didn't know at first how I want to - or can - tell about the phase of the project I am in and how to show questions I was asking myself. Storytelling lectures gave a glimpse into the world of creating engaging narratives and that gave me an idea to literally ask those questions - after all it was a session of dialogues with people working in and outside of the fields so instead of a form of just "show + get a feedback" I created something that I hoped would be "ask question + start conversation". And indeed I have now a feeling that this way really paid off, as I found a lot of interesting insights during the talks and after all, that was the point.


___
##Presentation

For the term review and our Design Dialogues session I prepared a stand with samples in Petri dishes (as presented below) and corresponding questions.


![](assets/bacteria-01.png)

![](assets/bacteria-02.png)

![](assets/bacteria-03.png)


I also viewed slides with main resources that either helped me learn about bacteria, narrowed my research or were projects with similar topic. Resources can be seen here:

<div data-configid="23554815/68533634" style="width:525px; height:372px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>


___
##Feedback session


This session and conversations were very insightful. Both expertise from people dealing with this topic on their daily work, especially like [Nuria Conde](https://iaac.net/dt_team/nuria-conde/), as well as interlocutors who may be not looking into these areas in their work, but have their own open perspectives and therefore new and refreshing paths for thoughts for me.

**Most of the conversations can be divided into three categories:**


- how to be part of the existing trend, add own drop in such issues or fields as breeding or growing matter (for example, "leather from kombucha), manipulation of bacteria (for example, dyeing using them); how to join the existing interventions, be part of a large whole,  and what is interesting here for me it that already at that time there are many "cookbooks", recipes, researches - there is a very large and easily accessible database on which one can or could base the work and project, using it all as references, a start point where someone saw a finish line, generally inspire yourself.

- how to design for the coexistence of bacteria and human (which is in a way obvious, because the bacteria live inside us, on our skin, are present in our environment, in food but it's quite often not taken into consideration) especially when we spend so much time actually harming our private biomass without knowing it or ignoring it, instead of embracing it and living in symbiosis; how to think about bacteria as a tool, but working together with a human being - or on a human being, exhorting what already exists (for example cosmetic products that interact with bacteria on human skin).

- how to use bacteria, especially those friendly to humankind or existing in / on it to create, or rather to grow, modify, cultivate an extension for humans (which for some reason I associate with Xenomorph from the movie Alien vs. Predator, although after all this isn't the point nor the direction) that could exist inside the body or as a separate, external being. Aesthetically and designally, such an area would go towards [Neri Oxman's](http://www.materialecology.com/projects.html) work, but, as Nuria pointed out, such a research area, and especially a hypothetical final project which indeed could at least partially work and not be just a concept or speculation, would be a fresh approach to the topic of bio-design, an intervention that is still absent today.



Throughout this semester, especially after defining my wheel of interests, I fought with a certain "anxiety of choice", that is, focusing on one or two of the areas and reflecting on it and filtering it through the upcoming and former classes. I focused on the topic of "bio" a bit randomly, or rather without a specific belief that this is the only topic, while wondering how I can include other emerging in my mind interest, i.e. working with children or more specific even, designing for children's culture. And as trivial as it sounds now, I had to hear it from Mariana, who works with FabKids - a relatively simple answer to my thoughts. Namely using for example the methodology of sets that teach programming or robotics and transferring such solutions to a bio-learning kit for children.



For the upcoming break I want to "digest" all the observations, feedbacks and insights and think about how to narrow my area of intervention for this master project, but in part I feel that I will focus on the issue of bio-design in the context of children's education. In the next trimester I would like to define this context more precisely and find a "weak point" in which such intervention would make sense and could bring possible future good and meaningful - even if in a micro-scale - changes.
