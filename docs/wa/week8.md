#**WEEK 08. Living with ideas**
*by Angella Mackey and David McCallum*



##19-11-18: Creating a speculative artefact

During this task, we had to choose an everyday object brought by one of the classmates. Then, based on this object, we had to ask a "what if" question, trying to create a speculative scenario about the future. My item was a plastic toothbrush. First, I defined the areas that I associate with both the object itself and the activity and space which it bound up with. Based on this, I asked a series of questions. Then I prototyped the object. The next step was to deal with this artefact and get into a situation where it would be really used in a world where the question would be real.


**AREAS:**

- sensibility
- sensuality around and in mouth
- rituals
- tactility
- haptic experience of materials, structure, texture
- private zone
- areas of body it may and may not touch


**QUESTIONS:**

What if haptic feeling would disappear? How can we train touch and keep it alive? What if we would need a new ritual added to our day in order to train our senses? What if the act of brushing teeth changes into really sensual and even erotic experience?


**PROTOTYPE:**

![](assets/tooth.jpg)


**REFLECTIONS:**

This prototype helped by showing how it is to focus on the texture, let me think about the materiality of everyday object, it let me notice the intimate relationship we may create with objects that are parts of our rituals, how stimulating senses can become a private experience when it comes to touching, how often are we loosing focus on our senses.


**GENERAL CLASS REFLECTIONS**

Did it help you to be more specific? to refine some aspect?
Did it help to involve yourself personally?
Did something happen that you did not expect?

OUR ANSWERS:

- helps to make it more concrete
- helps finding errors or flaws in existing tools
- shows how body language can be transformed and new gestures be created
- shows longevity and frequency of use in context
- explores individual and intimate rituals in our private sphere
- helps to imagine consequences
- shows how the context will change
- lets experience a potential of real feeling that people can have
- helps to find what triggers you to be emotionally connected
- shows how to relate physical forms to speculative scenarios
- helps to find all the possible situations where your speculation will not work
- lets understand what the meaning of your actions is
- lets position thoughts on the body, clarify the ritual
- helps to find different aspects or elements to take into account
- allows to get feedback from others
- shows how to express information
- creates a new form of communication, a new language, interactions

EXTRA KNOWLEDGE GAIND THROUGH THIS EXERCISE:

- to trust your own ideas, instinct, skills, knowledge
- to believe in yourself and your project
- to experiment and iterate with your project with existing limitations of the real world
- to prototype a technology, object, situation that doesn't yet exist


___
##20-11-18: Wearing the digital

This day's task was to experiment with greenscreen app (for instance [ChromaKey Studio Pro](https://itunes.apple.com/us/app/chroma-key-studio-pro/id952792265?mt=8) or [Veescope](https://itunes.apple.com/us/app/veescope-live-green-screen-app/id568390801?mt=8)) by wearing / using green fabrics or tapes and projecting mapping) videos and clips on them, imaging the possible situation when instead of having such object only on the screen, we would have a real digital dynamic fabric. First question was to describe what would we wear, then play with fabrics, construction and clips and at the end collect reflection.


**WHAT WOULD YOU WEAR?**

- sunglasses or something covering the face - as in the thinking that eyes / face is the mirror to your soul
- kimono / cape - because of the dynamism of the clothing form itself
- social situations, when I need to manifest something - like lack of will to get attention or the other way around, to communicate something
- or maybe I wouldn't put fabric, but color the skin and use the skin/body as digital screen?


**PLAY**

<iframe width="560" height="315" src="https://www.youtube.com/embed/xXHnPUqCAvc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/3oUFyMtaTic" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**REFLECTIONS**

I focused on covering my face, playing with different materials and their transparency (which in the end was not as visible in the film as I expected, but I guess it results of the lack of knowledge of the software), on the form of the face covering attached to the item of "everyday use", a well know object (eyewear). Initially I planned to cover the places where normally the lenses are put. In the end I pinned the form that made me think of the fascinator usually worn on heads. The second recording was a one-colored tape which I put on the skin. The video shows classmates, it is displayed "live" (sort of). It was quite a specific feeling - thinking of the skin as a fabric, as a canvas on which you can paint the interface or screen.

Here are some thoughts on the task:

- Experiencing the project by mimicking the probable, possible situation (in this case - technology) allows a completely new reception of the interface, new experiences and a change of perspective, but the hour time we had to complete the task is not much - in order to have good results, we should perhaps first learn the program better and additionally stop "having fun" and start looking at the project a bit more objectively - it's hard to get out of the playfulness zone at the beginning.

- I felt a bit uncomfortable recording someone else without one's knowledge and I would probably feel uncomfortably knowing and finding out that someone "hacked" me and my fabric.

- It was surprisingly easy (in the meaning: available?) to pretend non-existent technology and embody it so quickly and prototype-ly; it's definitely a method that I'll use in future projects, as it can already give some meaningful insights, help understand the project and the context better but also point some flaws and unexpected errors (that at the end may turn to be values).

- At the beginning I didn't expect that making the video and then later re-watching the film from the process of prototyping technology will give me such a strong impression - I mean that when after the classes I uploaded the video to the site and watched it once again, I began to keep catching myself on thinking that what I wore on the face could actually be seen by someone as digital texture in real time, that particular day, as if it was there, already existing.

- making and uploading selfies is hard, even though I needed it just for researching and educational purposes.


 **GENERAL CLASS REFLECTIONS**

How did it feel to do this?
What was your focus, what was the most important thing for you?
Did something happen that you did not expect?
How different was your experience of wearing, from what you predicted?

OUR ANSWERS:

- it was hard to get past the “gimmick” phase in only 1 hour
- when trying to wear what you predicted, it didn’t feel right at the end (for example - a project showing emotions: would I
really want someone to have information about my emotions? When confronted with this, it wasn't a desirable effect)
- it felt strange to be greenscreened without knowing, without permission
- it was different to do the thing than to imaging doing the thing

EXTRA KNOWLEDGE GAIND THROUGH THIS EXERCISE:

- technology doesn’t have to be fully developed to start trying to understand it
- knowing what is the essence of idea, what parts can be mimicked, what actions can be taken helps creating such a speculative artefact, but it works the other way around as well - creating an object helps finding answers to those questions
- everyone is a person with a real life experience - "me" and "my experience" can be used to learn something and draw some reflections


 ____
##21-11-18: Making a magic machine

**TASK**

The work with my magic machine was kind of specific - it didn't take place during classes. After the lesson, I tried to ask for some advice on how to approach the topic, but everything I heard started with the words: "it's hard to explain" so the more I wonder what kind of reflection and feelings I will have after trying to accomplish the task.

My manual to build a magic machine aka information I gathered on how to do it:

- select from the list the need and build a machine that will respond to its need
- the machine can not be an illustration of the need
- the machine must contain "magic" (which I interpreted as an inexplicable mode of action / feature / reaction)
- building a scenario or asking "what if" can help
- the machine should be given a name


**NEED**

The need I choose was PHYSICAL ACTIVITY and that is the need to move the body. It resonates in long distance with the topic around which I would like to look for the subject of my future work, additionally physical culture is a topic of my interests and nowadays I look with curiosity at the development of design for sport and body culture.

*Physical activity is defined as any bodily movement produced by skeletal muscles that requires energy expenditure.*[https://www.who.int/dietphysicalactivity/pa/en/](source)


**THE MAGIC MACHINE I - SCAFANDER**

![](assets/scafander-01.jpg)


The need for movement is not always connected with the desire to move. Sometimes you feel the need to move, but what if cities are so compact that a man must become part of a huge sea of people and it's impossible to have enough space to move the body freely? The Scafander is the answer to this need. It has three levels of user influence: 0 - a delicate feeling of activity of selected muscles without causing any movement; 1 - active work of muscles that cause movement of the whole body without causing final sours; 3 - immediate fatigue, exhaustion of muscles, soreness without causing muscle movement.


**THE MAGIC MACHINE II - MOTUS**

![](assets/momentus.jpg)


Meeting the needs of physical activity and getting a sense of satisfaction and fatigue is a limited opportunity - because of the thing called "free time". What if the movement of one muscle area can interact and move through connections in the body to other parties, and thus give a sense of satiation with activity and movement? Motus is an object to meet the needs of body movement and ensuring fulfillment. Separated time for exercises is unnecessary because Motus fits in the hand, so its use can remain even invisible, and our satisfaction will be noticeable only through the smiling face and good attitude that physical activity guarantees.


**REFLECTIONS**

- Not knowing about whether I performed the exercise correctly and whether my subject really is a machine imposed a certain reluctance (though perhaps uncertainty and distance) on the task and on the interpretation of its effects.

- Building a machine that responds to a need helps to understand this need in the design sense (but of course not only, as a feature / process in general), to distribute it to prime factors or at least to draw out some parameters, which can later be used to implement in a project - such prototyping allows me to see the need from a different perspective (also - nowadays do we have that many of artefacts designed for purposes other than the desire to poses?).

- Introducing a fraction of "magic" (i.e. an inexplicable fragment, unspoken action) unlocked me from the thinking "first I have to do research on technology, see if I can build it" and let me focus on responding to the need.

- Although my objects are not cyberpunk machines with hundreds of gears, while taking pictures - in particular of Machine II - I devoted more time than usually on looking at the movement of my hand and the feeling of other muscles and movements that were associated with this activity.

- I wondered during the exercise how I would design such a magical machine if I wanted to use this method in the future, especially when creating projects regarding human interaction or between objects and people, but also projects that talk about the needs, so that I can see and investigate the reaction for a given machine, check the confidence and trust in the magic aka the unspoken. I imagine that it would be a very interesting experiment.


____
##22/23-11-18: Living with the idea

**DIALOG WITH THE CONTEXT**

These two days were a reflection on the previous exercises, new methods of prototyping and living with own ideas, becoming  own systems. They were very intense for me because they required more precise thoughts about my vision and areas of interest. To help myself get through this week efficiently, to (more or less) each of the areas of interest I described in one of last weeks I put the "what if" question, trying to provoke a hypothetical topic for future development.

![](assets/whatif.png)

In order to conduct a dialogue with the context and do the homework(1), I narrowed the areas and chose the topic of bio-organisms and creation of matter in the space of own house - bacteria as a tool for creating and making (having in mind the process of bacterial staining and the process of creating the kombucha material).

![](assets/whatif2.png)

(1) Homework was to mimic the hypothetical, not-yet-existing situation in the context of our questions and fields of interest to experience and discover particular issues from first-person perspective and creating the system of which we can be a part.


**THE IDEA**

Having asked the questions above, I started to plan my situation. I decided to create a stand mimicking biolab and to implement the usage of it in my evening and morning routine, trying to grow a "thing" so it can be ready to use day after / during next hours. In order to do that, I needed to do a really quick research on what can grow overnight and prepare (last-minute) shopping list.


**DO I HAVE ANY EXPECTETIONS?**

I expect the whole process may be really smelly, as we are speaking about natural organisms, processes and probably yeasts. I may fail conducting the "experiment" aka the growth - after all those are biochemical processes that although in the nature are going on without any supervising and can happen in many different conditions, even with disturbance, when done by human require precise control and very defined conditions (noted before starting the experiment).


**THE LAB**

I approached the project beginning with research. After a dozen open tabs in Chrome and few or so read publications, I felt that this was not the right way (at least, not working in this assignment). I thought that improvisation of the lab and carrying out several processes, and later giving meaning to the matter would bring me more advantages. I changed my "how. I carried out 3 processes. Below is their speculative description and the object that was to be made from them.

- creating an active foam that cleans the dishes through the contents of microbes, leaving a coating on them, which later - when in contact with food - helps us digest meals better.
- gel congealing process to make a band-aid which can be placed on even large wounds and so that by the bacteria contained in it, disinfect them and help blood curdle.
- dechlorination of water(2) which should be properly prepared before consumption.

(2) placed as a process that I do today without realizing that "airing of tap water" is already a certain bio-lab oriented task - it is something "usual" in this whole hypothetical statement.


![](assets/mylab1.jpg)

![](assets/mylab2.jpg)



**REFLECTIONS**

Conducting biochemical process, even a fake one and on-the-spot requires huge knowledge. Cultivating and growing the matter requires rather strict schedule and rigid planning which leads to changing the habits and day-to-day routines - the action of growing introduces new activity during the day and it must be planned beforehand. In this short period of time it was also inconvenient, mostly because of trying to come up with a process that will give some effects over night (at the beginning, before changing the approach) and having to make shopping in order to be able to prepare some samples. I put The Lab in the kitchen, as I assumed it will be the most handy place - it has warm and hot water, possibility to heat something, cool it down, freeze and generally the eventuality to work with different states of concentration of the material. Introducing a new object to my kitchen felt partly exciting, partly annoying - it takes time to get used to changes and such an item interfered with the space. Overall the whole task made me focus more on what kind of materials and rituals are used around our home and think about where the materials come from (noted right after finishing first steps of the experiment and seeing first material results).


**WHAT PREVENTS YOU FROM MOVING FURTHER?**

Noted after conversation in class with group of similar, bio- and material-oriented interests:

- Second guessing my own work, project, idea and general approach towards problem-solving, working and prototyping and putting thoughts in words.
- Time scale - projects which are about process, about growing, changing the state, are time-consuming, and putting it really simple - those projects take time. They have to be pre-planned and applied in the given time.
- Life cycle of organisms and natural actors - organic matter has its own unique life cycle. In terms of having a lab and cultivating things that could be then use on daily basic around the house (not only for example as textile, but also as detergents or cosmetics) such a thing is crucial to take into consideration. Growth takes only that part of the time, spoiling takes that particular amount of time. In the scale of this homework, time was limitation and wasn't really on my side.
- Knowledge is the main ingredient - from user side: even with really detailed manual on how to grow something, some knowledge is requires, because maybe if something goes wrong we need to be aware of that and not use the "laborated" object because it may be dangerous / on the side of the creator the knowledge is the basic - understanding how chosen organism works, what kind of condition it needs, how it can react with other factors, predicting what can go wrong and understanding risks.. the list can go on and on. So the questions, as almost always, is: how to find the right balance? In the scale of homework it was the lack of knowledge and skills that was absent and really disturbing and blocking - I could have spent the whole night researching and still have no clue what organism I can work with to really have a wished result.


**POINTS TO REMEMBER**

- Understanding that I don't need to jump to results right away
- Knowing when is the time to zoom in and when to zoom out - changing the perspective, looking either at the detail or the whole system, the context
- Trusting myself, trusting my ideas and intuition, relying on my skills, knowledge
- Understanding my own limitations and maybe taking advantage of them


___
##Watching the normal. Connecting the dots.

- Turning "normal", everyday situations into the object of study can help create quite a new, unique insight and after, new kind of manual (manual for researching, for testing, but also for understanding rituals, actions, undertakings etc.)

- Designing for futures is in a way similar to surfing - you don't go for a huge weave first. You would most probably die there. Instead, you learn to swim on almost no-weave conditions, sliding on the movement of the broken weave, then catching white weaves, going for bigger and more aggressive every time. In design, operating with small weaves can help push the bigger one. The similarity is in the conclusion - you don't go for big weaves first. Getting out of the marine vocabulary, there is no need to be afraid of taking small, even micro actions as they are what usually amplify huge changes - in order for great things to happen, we need to have an alignment of smaller actions first.

- I keep feeling some sort of "designing anxiety" and by that I mean that I keep thinking, especially since I study "Design for emergent future" in order to actually shape a good kind of future, that my projects, actions and (upcoming) interventions are simply not enough. But the output of this week workshops and Monday's wrap-up is, besides learning new methods, also that starting in small scale we can already create a change - and sometimes that means putting something out there - there in the world - and giving people an opportunity to iterate on my own project. Designing for the future is basically a continuous process. Being fearless about the future starts by engaging now.

- Another topic full of anxieties is the fear of choosing. We've been pushed the previous weeks to describe our fields of interest, areas of intervention and I'm catching myself on having problem with choosing one topic, as if it will something that defines my whole life (it's obviously not). The great example was given by Mariana. One of the colleagues was having a trouble choosing between the two topics - dancing and Arabic writing. It was pointed out that actually they can be put in one box. Both of those actions are the actions of body movement and the way of expressing. And I can think of many more. The point is that they have common denominator, even though on the spot they may seem very different. And that's the story of connecting dots - if one's really interested in something and have the intuition to choose those particular subjects the thing is to find some similarities, common areas or the other way around - something that could work as contrast.
