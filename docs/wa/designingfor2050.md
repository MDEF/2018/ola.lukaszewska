#Designing for 2050

![](assets/wtf-11.jpg)

**INTRODCUTION:**
IAM - https://www.internetagemedia.com/ ; The alternative think-tank, consultancy and community, exploring the evolution of internet cultures and the influence of digital technologies in the futures of citizens and the Planet; main concepts: critical optimism / planetary narratives, thinking that everything is connected, thinking in a big scale, long-term strategies and long-term thinking in different scales.
_________________________________________

![](assets/wtf-15.jpg)

![](assets/wtf-12.jpg)

<iframe width="1519" height="537" src="https://www.youtube.com/embed/_SvON89JxwM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



_____________________________
##Takeouts


![](assets/wtf-13.jpg)



<iframe src="https://giphy.com/embed/YlQQYUIEAZ76o" width="480" height="359" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/traffic-television-animated-YlQQYUIEAZ76o">via GIPHY</a></p>

_______________
##Heterotropia

Heterotopia is a concept elaborated by philosopher Michel Foucault to describe certain cultural, institutional and discursive spaces that are somehow ‘other’: disturbing, intense, incompatible, contradictory or transforming. Heterotopias are worlds within worlds, mirroring and yet upsetting what is outside.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qFcxzoPbJis" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


_________
##Thoughts


being there instead of getting there

drawing attention to space

spaces like identity are constructed


My main takeout from the Design for 2050 classes is not about the futures, but about "here and now", namely about collectiveness and methodologies for structuring the class. During the meetings we did a lot of short, quick exercises, like collectively drawing faces of the person of the future but also creating the space and moving tables in preparation for another class. Collectiveness can be randomize and it certainly can be playful, even while speaking about profound and huge topics. Besides, playfulness was also a big part of those classes - who would thought that the whole presentation can be constructed by just gifs and still tell a decent story and be a background for really valuable lessons. Communication is the key and sometimes the means don't have to be high-quality produced videos to still carry the proper message.


___________
##Food for thoughts

https://howwegettonext.com/exploring-the-future-without-cyberpunks-neon-and-noir-8e23562819e3
